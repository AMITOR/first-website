$(document).ready(function(){
  
  var turn="X";
  var turns=["#","#","#","#","#","#","#","#","#"];
  var computersTurn="O";
  var gameOn=false;
  var count=0;
  var check=true;
  var pla1="X";
  
  
  $("#p1").click(function(){
    turn="X";
    computersTurn="O";
    reset();
    $("#p1").addClass("but1");
    $("#p2").removeClass("but1");
    $("#men1").removeClass("del");
    $("#men2").addClass("del");
    check=true;
  });
  
  $("#p2").click(function(){
    turn="X";
    computersTurn="O";
    reset();
    $("#p2").addClass("but1");
    $("#p1").removeClass("but1");
    $("#men2").removeClass("del");
    $("#men1").addClass("del");
    check=false;
  });
  
  $("#turnX").click(function(){
    turn="X";
    computersTurn="O";
    $("#turnX").addClass("but1");
    $("#turnO").removeClass("but1");
    reset();
  });
  
  $("#turnO").click(function(){
    turn="O";
    computersTurn="X";
    $("#turnO").addClass("but1");
    $("#turnX").removeClass("but1");
    reset();
  });
  
  $("#tuO").click(function(){
    turn="O";
    pla1="O";
    computersTurn="X";
    $("#tuO").addClass("but1");
    $("#tuX").removeClass("but1");
    reset();
  });
  
  $("#tuX").click(function(){
    turn="X";
    pla1="X";
    computersTurn="O";
    $("#tuX").addClass("but1");
    $("#tuO").removeClass("but1");
    reset();
  });
  
  $("#reset").click(function(){
    reset();
  })
  
  function computerTurn(){
    var taken=false;
    while(taken===false && count!==5){
      var computersMove=(Math.random()*10).toFixed();
      var move=turns[computersMove];
      if(move==="#"){
        $("#"+computersMove).addClass(computersTurn);
        taken=true;
        turns[computersMove]=computersTurn;
      }
    }
  }
  
  function playerTurn(tur, id){
    var spotTaken = turns[id];
    if(spotTaken==="#" && gameOn===false){
      count++;
      turns[id]=tur;
      $("#"+id).addClass(tur);
      winCondition(turns, tur);
      if(gameOn===false){
        if(check){
        cpuTurn(turns);
        winCondition(turns, computersTurn);
        }
        else{
          if(tur==="X")turn="O";
          else turn="X";
        }
      }
    }
  }
  
  
  
  function winCondition(turns, turn1){
    if(turns[0]===turn1 && turns[1]===turn1 && turns[2]===turn1){
      gameOn=true;
      $("#0").addClass("won");
      $("#1").addClass("won");
      $("#2").addClass("won");
    }
    else if(turns[3]===turn1 && turns[4]===turn1 && turns[5]===turn1){
      gameOn=true;
      $("#3").addClass("won");
      $("#4").addClass("won");
      $("#5").addClass("won");
      
    }
    else if(turns[6]===turn1 && turns[7]===turn1 && turns[8]===turn1){
      gameOn=true;
      $("#6").addClass("won");
      $("#7").addClass("won");
      $("#8").addClass("won");
      
    }
    else if(turns[0]===turn1 && turns[3]===turn1 && turns[6]===turn1){
      gameOn=true;
      $("#0").addClass("won");
      $("#3").addClass("won");
      $("#6").addClass("won");
      
    }
    else if(turns[1]===turn1 && turns[4]===turn1 && turns[7]===turn1){
      gameOn=true;
      $("#1").addClass("won");
      $("#4").addClass("won");
      $("#7").addClass("won");
      
    }
    else if(turns[2]===turn1 && turns[5]===turn1 && turns[8]===turn1){
      gameOn=true;
      $("#2").addClass("won");
      $("#5").addClass("won");
      $("#8").addClass("won");
      
    }
    else if(turns[0]===turn1 && turns[4]===turn1 && turns[8]===turn1){
      gameOn=true;
      $("#0").addClass("won");
      $("#4").addClass("won");
      $("#8").addClass("won");
      
    }
    else if(turns[2]===turn1 && turns[4]===turn1 && turns[6]===turn1){
      gameOn=true;
      $("#2").addClass("won");
      $("#4").addClass("won");
      $("#6").addClass("won");
    
    }
    else{
      gameOn=false;
    }
    if(gameOn && check){
      if(turn1===turn)$("#msg").text("You Won the Game!");
      else if(turn1===computersTurn)$("#msg").text("You lost the Game!");
    }
    else if(gameOn){
      if(turn1===pla1)$("#msg").text("Player1 won the Game!");
      else $("#msg").text("Player2 won the Game!");
      }
    else if(count===9)
      {
        $("#msg").text("It was a Tie!");
      }
  }
  
  $(".game-cell").click(function(){
    var slot=$(this).attr('id');
    playerTurn(turn, slot);
  });
  
  function reset(){
    turns=["#","#","#","#","#","#","#","#","#"];
    count=0;
    $(".game-cell").removeClass("X");
    $(".game-cell").removeClass("O");
    $(".game-cell").removeClass("won");
    gameOn=false;
    $("#msg").text("");
  }
  
  function isMovesLeft(temp){
    var i;
    for(i=0;i<9;i++){
      if(temp[i]=="#"){return true;}
    }
    return false;
  }
  
  function evaluate(temp){
    
    if(temp[0]===temp[1] && temp[1]===temp[2]){
      if(temp[0]===computersTurn) return +10;
      else if(temp[0]===turn) return -10;
    }
    
    if(temp[3]===temp[4] && temp[4]===temp[5]){
      if(temp[3]===computersTurn) return +10;
      else if(temp[3]===turn) return -10;
    }
    
    if(temp[6]===temp[7] && temp[7]===temp[8]){
      if(temp[6]===computersTurn) return +10;
      else if(temp[6]===turn) return -10;
    }
    
    if(temp[0]===temp[3] && temp[3]===temp[6]){
      if(temp[0]===computersTurn) return +10;
      else if(temp[0]===turn) return -10;
    }
    
    if(temp[1]===temp[4] && temp[4]===temp[7]){
      if(temp[1]===computersTurn) return +10;
      else if(temp[1]===turn) return -10;
    }
    
    if(temp[2]===temp[5] && temp[5]===temp[8]){
      if(temp[2]===computersTurn) return +10;
      else if(temp[2]===turn) return -10;
    }
    
    if(temp[0]===temp[4] && temp[4]===temp[8]){
      if(temp[0]===computersTurn) return +10;
      else if(temp[0]===turn) return -10;
    }
    
    if(temp[2]===temp[4] && temp[4]===temp[6]){
      if(temp[2]===computersTurn) return +10;
      else if(temp[2]===turn) return -10;
    }
    
    return 0;
  }
  
  function minimax(temp, depth, isMax){
    var score = evaluate(temp);
    if(score===10) return score;
    if(score===-10) return score;
    if(isMovesLeft(temp)===false) return 0;
    if(isMax){
      var best = -1000;
      var i;
      for(i=0;i<9;i++){
        if(turns[i]==="#")
          {
            temp[i]=computersTurn;
            best=Math.max(best, minimax(temp, depth+1, !isMax));
            temp[i]="#";
          }
      }
      return best;
    }
    
    else{
      var best = 1000;
      var i;
      for(i=0;i<9;i++){
        if(temp[i]==="#"){
          temp[i]=turn;
          best=Math.min(best, minimax(temp, depth+1, !isMax));
          temp[i]="#";
        }
      }
      return best;
    }
  }
  
  function cpuTurn(temp){
    
    var bestVal = -1000;
    var req = -1;
    var i;
    for(i=0;i<9;i++){
      if(temp[i]==="#"){
        temp[i]=computersTurn;
        var moveVal=minimax(temp, 0, false);
        temp[i]="#";
        if(moveVal >= bestVal){
          bestVal = moveVal;
          req = i;
        }
      }
    }
    $("#"+req).addClass(computersTurn);
    turns[req]=computersTurn;
    count++;
  }
  
});
